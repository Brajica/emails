class UsermailerMailer < ApplicationMailer   
	default From: "jimenezcardenasb@gmail.com"
   def welcome_email(user)
      @user = user
      @url  = 'idm-w.herokuapp.com'
      mail(to: @user.email, subject: 'Gracias por acceder a idm-work')
   end
end
